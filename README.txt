Version 1.0 (maj le 16/01/2018)

Pr�requis : 
- Avoir Bluestacks install� ainsi que Lol Champion Manager
- Au moins la version de Java 7 doit �tre install�

/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\
- Durant l'exc�cution du script, la souris ne fonctionne plus correctement et la fenetre de Lol Champion Manager doit en permanance rester visible
- Ctrl + F1 : arrete le script et laisse la partie se d�rouler jusqu'� la fin
- Alt + Maj + C : arrete imm�diatement le script
/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\

I] T�l�charger SikuliX comme indiqu� ici : http://sikulix.com/quickstart/
	1) Il faut t�l�charger le fichier sikulixsetup-1.1.1.jar depuis https://launchpad.net/sikuli/sikulix/1.1.1
	2) Faire un dossier dans Documents ou Bureau qui contiendra toutes les fichiers n�cessaires au bon fonctionnement de SikuliX (le chemin de ce fossier ne doit pas contenir d'espace ou de caract�res sp�ciaux)
	3) Dans ce dossier copier/coller sikulixsetup....jar
	4) Double click sur sikulixsetup....jar
	5) Lors de l'installation, choisir l'option 1 avec la checkbox Python de coch�e

II] Lancer l'IDE SikuliX en double cliquant sur runsikulix.cmd

III] Ouvrir le dossier contenant le script scipt_LoL_Manager.sikuli depuis l'IDE SikuliX

IV] Ouvrir Lol Champion Manager depuis Bluestacks et le mettre en plein �cran. En cas de multi �crans, il doit �tre lanc� sur l'�cran PRINCIPAL !!!

V] Dans la barre de t�ches de l'IDE SikuliX, cliquer sur le bouton Lancer. Le script va se lancer automatiquement et les parties normales vont s'enchainer !
